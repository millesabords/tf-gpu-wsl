import tensorflow as tf
import datetime

tf.debugging.set_log_device_placement(True)

t1 = datetime.datetime.now()
t2 = datetime.datetime.now()
try:
  with tf.device('/device:CPU:0'):
    a = tf.constant([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]])
    b = tf.constant([[1.0, 2.0], [3.0, 4.0], [5.0, 6.0]])
    t2 = datetime.datetime.now()
    print("Single CPU computation time: " + str(t2-t1))

    t1 = datetime.datetime.now()
  with tf.device('/device:GPU:0'):
    c = tf.matmul(a, b)
    tf.print(c)
    t2 = datetime.datetime.now()
    print("Single GPU computation time: " + str(t2-t1))
except RuntimeError as e:
  print(e)
